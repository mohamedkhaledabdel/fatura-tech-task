# README #

steps necessary to get the application up and running.

### What is this repository for? ###

* REST APIs implementation

### How do I get set up? ###

* Open microsoft sql serven and connect to it
* open visual studio
* Configure the database datasource in connection string in the Web.config file to the local DB
	* <add name="EshopContext" connectionString="data source=DESKTOP-7GKSP8U\SQLEXPRESS;initial catalog=EshopDB;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework" providerName="System.Data.SqlClient" />
* In visual studion, type the following commands in the package manager console which will lead to create tables and seed data
	* enable-migrations
	* update-database
* Build and run the project
* Start consuming the APIs
	* View all products (https://localhost:44332/api/products?&pageNumber=1&pageSize=3)
	* View all products under a certain category (https://localhost:44332/api/products/category/00E00BD7-3F2D-4245-B80B-12F03114B24E)

### What extra implementaion I could have done ? ###

* Add extra layer of seperation between controllers and business
* Add logging
* Containerize the App
* Deploy it on Azure
* Implement front end using React.js
* Writing unit tests
* Automated tests using selenium