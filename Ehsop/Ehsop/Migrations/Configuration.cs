namespace Eshop.Migrations
{
	using Eshop.DAL;
	using System;
	using System.Data.Entity;
	using System.Data.Entity.Migrations;
	using System.Linq;

	internal sealed class Configuration : DbMigrationsConfiguration<EshopContext>
	{
		public Configuration()
		{
			AutomaticMigrationsEnabled = false;
		}

		protected override void Seed(EshopContext context)
		{
			EshopContextSeedInitializer init = new EshopContextSeedInitializer();
			init.SeedCall(context);
		}
	}
}
