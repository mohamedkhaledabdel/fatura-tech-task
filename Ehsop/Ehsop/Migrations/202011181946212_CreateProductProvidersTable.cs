﻿namespace Eshop.Migrations
{
	using System;
	using System.Data.Entity.Migrations;

	public partial class CreateProductProvidersTable : DbMigration
	{
		public override void Down()
		{
			DropForeignKey("dbo.ProductProvider", "ProductID", "dbo.Product");
			DropIndex("dbo.ProductProvider", new[] { "ProductID" });
			AlterColumn("dbo.Product", "Price", c => c.Int(nullable: true));
			DropTable("dbo.ProductProvider");
		}

		public override void Up()
		{
			CreateTable(
				"dbo.ProductProvider",
				c => new
				{
					ID = c.Guid(nullable: false),
					Name = c.String(),
					Price = c.Int(nullable: false),
					ProductID = c.Guid(nullable: false),
				})
				.PrimaryKey(t => t.ID)
				.ForeignKey("dbo.Product", t => t.ProductID, cascadeDelete: true)
				.Index(t => t.ProductID);

			AlterColumn("dbo.Product", "Price", c => c.Double(nullable: true));
		}
	}
}
