﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Eshop.Models
{
	public class User
	{
		[Required]
		public string Address { get; set; }

		[Required]
		[DataType(DataType.Date)]
		public DateTime BirthDay { get; set; }

		[Required]
		[EmailAddress]
		public string Email { get; set; }

		[Required]
		public string FirstName { get; set; }

		[Key]
		public Guid ID { get; set; }

		[Required]
		public string LastName { get; set; }

		[Required]
		public string MiddleName { get; set; }

		[Required]
		[DataType(DataType.Password)]
		public string Password { get; set; }
	}
}
