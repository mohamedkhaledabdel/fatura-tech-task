﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ehsop.Models
{
	public class PagingModel
	{
		private const int maxPageSize = 25;

		public int _pageSize { get; set; } = 10;
		public int pageNumber { get; set; } = 1;

		public int pageSize
		{
			get { return _pageSize; }
			set
			{
				_pageSize = (value > maxPageSize) ? maxPageSize : value;
			}
		}
	}
}
