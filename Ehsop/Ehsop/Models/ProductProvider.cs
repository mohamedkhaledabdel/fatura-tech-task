namespace Ehsop.Models
{
	using Eshop.Models;
	using System;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Data.Entity;
	using System.Linq;

	public class ProductProvider
	{
		[Key]
		public Guid ID { get; set; }

		public string Name { get; set; }

		public int Price { get; set; }

		//Using virtual keyword will cause the category object itself to be returned with other properties.
		//This concept is called lazy loading
		public virtual Product Product { get; set; }

		[ForeignKey("Product")]
		public Guid ProductID { get; set; }
	}
}
