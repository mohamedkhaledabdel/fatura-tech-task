﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Eshop.Models
{
	public class Category
	{
		[Key]
		public Guid ID { get; set; }

		public string Name { get; set; }
		public virtual ICollection<Product> Products { get; set; }
	}
}
