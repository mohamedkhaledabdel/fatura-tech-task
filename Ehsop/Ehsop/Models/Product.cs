﻿using Ehsop.Models;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Eshop.Models
{
	public class Product
	{
		public Category Category { get; set; }

		[ForeignKey("Category")]
		public Guid CategoryID { get; set; }

		[Key]
		public Guid ID { get; set; }

		public string Name { get; set; }

		public double Price { get; set; }

		public virtual ICollection<ProductProvider> ProductProviders { get; set; }
	}
}
