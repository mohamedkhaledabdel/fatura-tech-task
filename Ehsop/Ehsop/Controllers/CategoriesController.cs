﻿using Eshop.DAL;
using Eshop.DAL.Repositories;
using Eshop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;

namespace Ehsop.Controllers
{
	public class CategoriesController : ApiController
	{
		private ICategoryRepository categoryRepository;

		private CategoriesController(ICategoryRepository categoryRepository)
		{
			this.categoryRepository = categoryRepository;
		}

		private CategoriesController()
		{
			this.categoryRepository = new CategoryRepository(new EshopContext());
		}

		// DELETE api/<controller>/5
		public void Delete(int id)
		{
		}

		[System.Web.Http.HttpGet]
		// GET api/<controller>
		public IEnumerable<Category> Get()
		{
			return categoryRepository.GetCategories();
		}

		// GET api/<controller>/5
		public string Get(int id)
		{
			return "value";
		}

		// POST api/<controller>
		public void Post([FromBody] string value)
		{
		}

		// PUT api/<controller>/5
		public void Put(int id, [FromBody] string value)
		{
		}
	}
}
