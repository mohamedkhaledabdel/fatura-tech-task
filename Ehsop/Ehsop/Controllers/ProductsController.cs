﻿using Ehsop.Models;
using Eshop.DAL;
using Eshop.DAL.Repositories;
using Eshop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

//using System.Web.Mvc;

namespace Eshop.Controllers
{
	[EnableCors(origins: "http://localhost:3000", headers: "*", methods: "*")]
	public class ProductsController : ApiController
	{
		private IProductRepository productRepository;

		private ProductsController(IProductRepository productRepository)
		{
			this.productRepository = productRepository;
		}

		private ProductsController()
		{
			this.productRepository = new ProductRepository(new EshopContext());
		}

		//Get api/products
		[HttpGet]
		public HttpResponseMessage GetProducts([FromUri] PagingModel paging)
		{
			var products = this.productRepository.GetProducts();
			//List<Product> productsResult = new List<Product>();

			foreach (Product product in products)
			{
				Guid idOfCheapestProvider = product.ProductProviders.OrderBy(p => p.Price).First<ProductProvider>().ID;
				product.Price = product.ProductProviders.FirstOrDefault<ProductProvider>(p => (p.ID == idOfCheapestProvider)).Price;
			}

			// Parameter is passed from Query string if it is null then it default Value will be pageNumber:1
			int CurrentPage = paging.pageNumber;
			// Parameter is passed from Query string if it is null then it default Value will be pageSize:20
			int PageSize = paging.pageSize;

			// Returns List of products after applying Paging
			var items = products.Skip((CurrentPage - 1) * PageSize).Take(PageSize).ToList();
			HttpResponseMessage response = Request.CreateResponse(System.Net.HttpStatusCode.OK, items);
			return response;
		}

		[Route("api/products/category/{categoryID}")]
		[HttpGet]
		public HttpResponseMessage GetProductsByCategoryID(Guid categoryID)
		{
			var products = this.productRepository.GetProductsByCategoryID(categoryID);
			foreach (Product product in products)
			{
				Guid idOfCheapestProvider = product.ProductProviders.OrderBy(p => p.Price).First<ProductProvider>().ID;
				product.Price = product.ProductProviders.FirstOrDefault<ProductProvider>(p => (p.ID == idOfCheapestProvider)).Price;
			}
			HttpResponseMessage response = Request.CreateResponse(System.Net.HttpStatusCode.OK, this.productRepository.GetProductsByCategoryID(categoryID));
			return response;
		}

		[HttpPut]
		public HttpResponseMessage UpdateProduct(Product product)
		{
			this.productRepository.UpdateProduct(product);
			this.productRepository.Save();
			HttpResponseMessage response = Request.CreateResponse(System.Net.HttpStatusCode.OK, "Updated successflly");
			return response;
		}
	}
}
