﻿using Ehsop.DAL.Repositories;
using Eshop.DAL;
using Eshop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Eshop.Controllers
{
	[EnableCors(origins: "http://localhost:3000", headers: "*", methods: "*")]
	public class UsersController : ApiController
	{
		private IUserRepository userRepository;

		private UsersController(IUserRepository userRepoistory)
		{
			this.userRepository = userRepoistory;
		}

		private UsersController()
		{
			this.userRepository = new UserRepository(new EshopContext());
		}

		[Route("api/user/{userID}")]
		public HttpResponseMessage GetUser(Guid userID)
		{
			HttpResponseMessage response = Request.CreateResponse(System.Net.HttpStatusCode.OK, this.userRepository.GetUser(userID));
			return response;
		}

		[HttpPut]
		public HttpResponseMessage UpdateUser(User user)
		{
			this.userRepository.UpdateUser(user);
			HttpResponseMessage response = Request.CreateResponse(System.Net.HttpStatusCode.OK, "Updated successfuly");
			return response;
		}
	}
}
