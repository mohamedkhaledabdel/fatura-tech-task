﻿using Ehsop.Models;
using Eshop.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Policy;
using System.Web;

namespace Eshop.DAL
{
	public class EshopContextSeedInitializer : DropCreateDatabaseIfModelChanges<EshopContext>
	{
		public void SeedCall(EshopContext context)
		{
			this.Seed(context);
		}

		protected override void Seed(EshopContext context)
		{
			Category cat1 = new Category { ID = Guid.NewGuid(), Name = "Mobile Phones" };
			Category cat2 = new Category { ID = Guid.NewGuid(), Name = "Wearable Devices" };
			Category cat3 = new Category { ID = Guid.NewGuid(), Name = "Laptops" };
			context.Categories.Add(cat1);
			context.Categories.Add(cat2);
			context.Categories.Add(cat3);
			context.SaveChanges();
			Product p1 = new Product
			{
				ID = Guid.NewGuid(),
				Name = "Iphone 11",
				CategoryID = cat1.ID,
			};
			Product p2 = new Product
			{
				ID = Guid.NewGuid(),
				Name = "Iphone 12",
				CategoryID = cat1.ID,
			};
			Product p3 = new Product
			{
				ID = Guid.NewGuid(),
				Name = "Iphone Max",
				CategoryID = cat1.ID,
			};
			context.Products.Add(p1);
			context.Products.Add(p2);
			context.Products.Add(p3);
			context.SaveChanges();
			ProductProvider pr1 = new ProductProvider
			{
				ID = Guid.NewGuid(),
				Name = "Provider 1",
				Price = 10000,
				ProductID = p1.ID
			};
			ProductProvider pr2 = new ProductProvider
			{
				ID = Guid.NewGuid(),
				Name = "Provider 1",
				Price = 11000,
				ProductID = p1.ID
			};
			ProductProvider pr3 = new ProductProvider
			{
				ID = Guid.NewGuid(),
				Name = "Provider 1",
				Price = 12000,
				ProductID = p2.ID
			};
			ProductProvider pr4 = new ProductProvider
			{
				ID = Guid.NewGuid(),
				Name = "Provider 1",
				Price = 12000,
				ProductID = p3.ID
			};
			context.ProductProviders.Add(pr1);
			context.ProductProviders.Add(pr2);
			context.ProductProviders.Add(pr3);
			context.ProductProviders.Add(pr4);
			context.SaveChanges();
			User u1 = new User
			{
				ID = Guid.NewGuid(),
				FirstName = "Mohamed",
				MiddleName = "Khaled",
				LastName = "Magdi",
				BirthDay = new DateTime(1993, 11, 26),
				Email = "mohamed.kh.abdelmeguid@gmail.com",
				Password = "123456",
				Address = "Palm Gardens Compound"
			};
			context.Users.Add(u1);
			context.SaveChanges();
		}
	}
}
