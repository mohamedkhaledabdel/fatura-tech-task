﻿using Eshop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eshop.DAL.Repositories
{
	internal interface IProductRepository
	{
		void DeleteProduct(int productID);

		Product GetProductByID(int productID);

		IEnumerable<Product> GetProducts();

		IEnumerable<Product> GetProductsByCategoryID(Guid categoryID);

		void InsertProduct(Product product);

		void Save();

		void UpdateProduct(Product product);
	}
}
