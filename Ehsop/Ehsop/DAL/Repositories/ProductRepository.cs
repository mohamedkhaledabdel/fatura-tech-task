﻿using Eshop.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Eshop.DAL.Repositories
{
	public class ProductRepository : IProductRepository, IDisposable
	{
		private EshopContext context;

		private bool disposed = false;

		public ProductRepository(EshopContext context)
		{
			this.context = context;
		}

		public void DeleteProduct(int productID)
		{
			Product product = context.Products.Find(productID);
			this.context.Products.Remove(product);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		public Product GetProductByID(int productID)
		{
			return this.context.Products.Find(productID);
		}

		public IEnumerable<Product> GetProducts()
		{
			return this.context.Products.ToList<Product>();
		}

		public IEnumerable<Product> GetProductsByCategoryID(Guid categoryID)
		{
			return (IEnumerable<Product>)this.context.Products.Where(prod => prod.CategoryID == categoryID);
		}

		public void InsertProduct(Product product)
		{
			this.context.Products.Add(product);
		}

		public void Save()
		{
			context.SaveChanges();
		}

		public void UpdateProduct(Product product)
		{
			//context.Entry(product).State = EntityState.Modified;
			var existingProduct = context.Products.Where(p => p.ID == product.ID).FirstOrDefault<Product>();
			if (existingProduct != null)
			{
				existingProduct.Name = product.Name;
				existingProduct.Price = product.Price;
			}
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!this.disposed)
			{
				if (disposing)
				{
					context.Dispose();
				}
			}
			this.disposed = true;
		}
	}
}
