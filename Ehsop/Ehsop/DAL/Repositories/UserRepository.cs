﻿using Eshop.DAL;
using Eshop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ehsop.DAL.Repositories
{
	public class UserRepository : IUserRepository
	{
		private EshopContext context;
		private bool disposed = false;

		public UserRepository(EshopContext context)
		{
			this.context = context;
		}

		public void DeleteUser(Guid userID)
		{
			User user = context.Users.Find(userID);
			this.context.Users.Remove(user);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		public User GetUser(Guid userID)
		{
			return this.context.Users.Find(userID);
		}

		public void InsertUser(User user)
		{
			this.context.Users.Add(user);
		}

		public void Save()
		{
			context.SaveChanges();
		}

		public void UpdateUser(User user)
		{
			var existingUser = context.Users.Where(u => u.ID == user.ID).FirstOrDefault<User>();
			if (existingUser != null)
			{
				existingUser.FirstName = user.FirstName;
			}
			this.Save();
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!this.disposed)
			{
				if (disposing)
				{
					context.Dispose();
				}
			}
			this.disposed = true;
		}
	}
}
