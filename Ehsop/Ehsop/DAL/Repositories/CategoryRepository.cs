﻿using Eshop.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Eshop.DAL.Repositories
{
	public class CategoryRepository : ICategoryRepository
	{
		private EshopContext context;

		private bool disposed = false;

		public CategoryRepository(EshopContext context)
		{
			this.context = context;
		}

		public void DeleteCategory(int categoryID)
		{
			Category cat = context.Categories.Find(categoryID);
			context.Categories.Remove(cat);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		public IEnumerable<Category> GetCategories()
		{
			return context.Categories.ToList<Category>();
		}

		public Category GetCategoryByID(int categoryID)
		{
			return context.Categories.Find(categoryID);
		}

		public void InsertCategory(Category category)
		{
			this.context.Categories.Add(category);
		}

		public void Save()
		{
			context.SaveChanges();
		}

		public void UpdateCategory(Category category)
		{
			context.Entry(category).State = EntityState.Modified;
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!this.disposed)
			{
				if (disposing)
				{
					context.Dispose();
				}
			}
			this.disposed = true;
		}
	}
}
