﻿using Eshop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eshop.DAL.Repositories
{
	internal interface ICategoryRepository
	{
		void DeleteCategory(int categoryID);

		IEnumerable<Category> GetCategories();

		Category GetCategoryByID(int categoryID);

		void InsertCategory(Category category);

		void Save();

		void UpdateCategory(Category category);
	}
}
