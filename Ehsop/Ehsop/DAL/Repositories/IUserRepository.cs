﻿using Eshop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ehsop.DAL.Repositories
{
	internal interface IUserRepository
	{
		void DeleteUser(Guid userID);

		User GetUser(Guid userID);

		void InsertUser(User user);

		void UpdateUser(User user);
	}
}
