namespace Eshop.DAL
{
	using Ehsop.Models;
	using Eshop.Models;
	using System;
	using System.Data.Entity;
	using System.Data.Entity.ModelConfiguration.Conventions;
	using System.Linq;

	public class EshopContext : DbContext
	{
		public EshopContext()
					: base("name=EshopContext")
		{
		}

		public DbSet<Category> Categories { get; set; }

		public DbSet<ProductProvider> ProductProviders { get; set; }

		// Your context has been configured to use a 'EshopDB' connection string from your
		// application's configuration file (App.config or Web.config). By default, this connection
		// string targets the 'Ehsop.DAL.EshopDB' database on your LocalDb instance.
		//
		// If you wish to target a different database and/or database provider, modify the 'EshopDB'
		// connection string in the application configuration file.
		public DbSet<Product> Products { get; set; }

		public DbSet<User> Users { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
		}
	}
}
